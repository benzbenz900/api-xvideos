var express = require('express');
const xvideos = require('./lib');
var cors = require('cors')
var cookieParser = require('cookie-parser')
const ExpressCache = require('express-cache-middleware')
const cacheManager = require('cache-manager')
const cacheMiddleware = new ExpressCache(
    cacheManager.caching({
        store: 'memory', max: 10000, ttl: 3600
    })
)

var app = express();
app.use(cors())
app.use(express.json());
app.use(cookieParser());

cacheMiddleware.attach(app)

app.get('/detail/*', async (req, res, next) => {
    const detail = await xvideos.videos.details({ url: `https://www.xvideos.com/${req.params[0]}` });
    res.json(detail)
})

app.get('/verified', async (req, res, next) => {
    const verified = await xvideos.videos.verified({ page: req.query.page ? req.query.page : 1 });
    res.json({
        videos: verified.videos,
        current: verified.pagination.current,
        pages: [...new Set(verified.pagination.pages)],
    })
});

app.get('/fresh', async (req, res, next) => {
    const fresh = await xvideos.videos.fresh({ page: req.query.page ? req.query.page : 1 });
    res.json({
        videos: fresh.videos,
        current: fresh.pagination.current,
        pages: [...new Set(fresh.pagination.pages)],
    })
});

app.get('/find/:k', async (req, res, next) => {
    const dashboard = await xvideos.videos.dashboard({ page: req.query.page ? req.query.page : 1, sc: req.params.k });
    res.json({
        videos: dashboard.videos,
        current: dashboard.pagination.current,
        pages: [...new Set(dashboard.pagination.pages)],
    })
});

app.get('/dashboard/:path', async (req, res, next) => {
    const dashboard = await xvideos.videos.dashboard({ page: req.query.page ? req.query.page : 1, PATH: req.params.path });
    res.json({
        videos: dashboard.videos,
        current: dashboard.pagination.current,
        pages: [...new Set(dashboard.pagination.pages)],
    })
});

app.get('/best', async (req, res, next) => {
    const best = await xvideos.videos.best({ year: new Date().getFullYear(), month: ('0' + new Date().getMonth()).slice(-2), page: req.query.page ? req.query.page : 1 });
    res.json({
        videos: best.videos,
        current: best.pagination.current,
        pages: [...new Set(best.pagination.pages)],
    })
});

module.exports = app;
