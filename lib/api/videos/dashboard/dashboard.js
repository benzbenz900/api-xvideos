const base = require('../../base');
const parseResponse = require('./parseResponse');

const dashboard = async ({ page = 1, PATH, sc } = {}) => {
  if (sc) {
    sc = encodeURI(sc)
    if (page < 1 || page > Number.MAX_SAFE_INTEGER) {
      throw new Error(`Invalid page: ${page}`);
    }
    const request = base.createRequest();
    return parseResponse(page, await request.get(`?k=${sc}&p=${page === 0 ? '' : page}`));
  } else {
    if (!PATH) {
      PATH = '/lang/thai';
    }

    if (page < 1 || page > Number.MAX_SAFE_INTEGER) {
      throw new Error(`Invalid page: ${page}`);
    }
    const request = base.createRequest();
    return parseResponse(page, await request.get(`/lang/${PATH}/${page === 0 ? '' : page}`));
  }
};

module.exports = dashboard;
