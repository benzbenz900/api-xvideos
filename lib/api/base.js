const axios = require('axios');

const BASE_URL = 'https://www.xvideos.com';
const createRequest = (options = {}) => {
  return axios.create({
    baseURL: BASE_URL,
    headers: { 
      'Cookie': 'cit=d4679782521ac6bdnvy-oX4uTHkFOiTDUk247w%3D%3D; session_token=c4e9507b29c63f5cHsJE4-CfjZf6sxyumGGH77LuYNdfArvV6sjrZUBA2Bzn-3ZocrhU4K1febijZXMQpoFhqCJCvtqs81FbRn77VeZy4d0YXgAVnLGHOKdh5UHXtylzfEJy97B-jdQTlw5RTx0MT7GDSOU_DwIDMzffL0cicHzdj82eLyr1VObmKWF2jFc_fVY0PAnLDLOOsovGJLo3qluAXQmlvvMdHwP-lXeiyhyGypQXS0ef3TszorI7Lj9Jk15V60DiHvvsG65c'
    },
    ...options
  });
};

const base = {
  BASE_URL,
  createRequest,
};

module.exports = base;
